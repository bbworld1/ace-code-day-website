---
title: "ACE Code Day 2021 | Information and Registration"
meta_description: "Join us for Pleasanton's largest STEM-based educational event."
date: 2019-03-26T08:47:11+01:00
draft: false

form_link: "https://docs.google.com/forms/d/e/1FAIpQLSevOIhQdz486bDLkWkDQ2zcZPZ68U-uELwvQIO42InUFdP_nA/viewform?embedded=true"

itinerary_entries:
  - 12:15 PM to 12:30 PM - Welcome Session
  - 12:30 PM to 1:30 PM - Track 1
  - 1:30 PM to 1:40 PM - Break
  - 1:40 PM to 2:40 PM - Track 2
  - 2:40 PM to 2:50 PM - Break
  - 2:50 PM to 3:50 PM - Track 3

workshops:
  - title: Build Your Own Computer - Fundamentals of Computer Hardware
    level: Beginner
    description: From scientific simulations to gaming, computers have revolutionized how we live. Pleasanton Partners in Education (PPIE) has graciously awarded ACE Coding a grant to develop this workshop for the students of Pleasanton. This workshop will cover all the fundamentals of assembling your very own desktop computer, from choosing the right components, to the building of the computer itself. We will cover what each component does, and how it fits into the broader picture of a complete computer system. This workshop serves as a good introduction into the world of computer hardware and electrical engineering. In addition, there will be a live demonstration of how to assemble a PC.
  - title: Intro to Version Control with Git
    level: Beginner
    description: Version control or source control is the practice of tracking and managing changes to code, an integral part of fast-paced and organized software development. The market leader for source control solutions is Git, a free and open source software. Git is the world's most popular system for developers to manage code and collaborate on projects. Learn how software companies, from small startups to Silicon Valley giants like Google and Facebook, manage and streamline their software development with Git. 
  - title: The Role of Aerodynamics in Aircraft Design
    level: Beginner
    description: Learn how to use a wind tunnel and computational fluid dynamics software to design and optimize the aerodynamics of a simple airplane body. Through hands on work, students will learn the principles of how aircraft fly, and how to make the aforementioned flight more efficient. Students could apply the skills learned as an aerospace engineer or even as a hobby, designing and building radio controlled planes.
  - title: Android App Development with Android Studio
    level: Beginner
    description: App development is one of the hottest fields today, and with the power of the Android, it's downright easy! In this short workshop, you'll learn to create your very own Android app from scratch. We'll give you the skills you need to take full advantage of the Android platform. Some previous coding experience is recommended.
  - title: Intro to CAD Modeling
    level: Beginner
    description: Computer Aided Design (CAD) Modeling is the backbone of mechanical engineering. It is through CAD that many of our world's most impactful inventions have been realized. It has revolutionized the field and makes innovation much faster and easier. CAD Modelling is fundamental in learning engineering, and this class will teach the basics, going over all the common commands in the part studio, teaching enough to get anyone to design most everyday objects. We will be using OnShape, a powerful and easy to use online CAD software.
  - title: Video Production - Making Your Own Videos!
    level: Beginner
    description: Wanna make your own videos? We can see all sorts of videos around us in this world wether its movie or just a youtube video. Video production is a handy skill that many students would love to have. It involves getting your own clips, cutting them, and then downloading it on our computer. Students in this workshop would be learning easy techniques of putting clips together, making minor adjustments or even just having fun! This skill could come in handy whether you want to make your own youtube videos or just simply have the capability to make fun videos. This workshop will require a mac book or a laptop but no skills required.
  - title: Intro to the Java Programming Language
    level: Beginner
    description: The world runs on code, and Java is one of the most widely-used programming languages out there. Learn what Java is, its applications, and how to write your first lines of code in the Fundamentals of Java Workshop! Java is powerful and easy to learn, and traditionally has served as the entry point for those new to coding. No experience needed **Recommended for first time coders and beginners.**
  - title: Intro to the Python Programming Language
    level: Beginner
    description: Looking to learn one of the most popular industry-standard programming languages today? Python is a readable, easy-to-learn programming language used by everyone from small startups to tech giants, with applications from automation scripts to web servers. Our Intro to Python workshop will get you started on the basics of using this incredibly powerful and popular language.
  - title: Intro to Virtualization and VMs
    level: Beginner
    description: Learn about Virtualization and how to start your own virtual machines with Oracle Virtual Box. Students will learn about hypervisors and different uses of Virtualization. Students can use Virtual Machines for testing out Operating Systems like Ubuntu, Centos, Hackintosh, and more. With different Operating Systems, students can test out programs that may be limited to a certain OS. Students can use their knowledge of virtualization to become a Virtualization Engineer.
  - title: Computer Vision and Object Recognition using Yolo
    level: Beginner
    description: Learn about how computers can "see"; using a state of the art machine learning AI algorithm called YOLO, students will learn the basics about how to develop their own model to detect various objects. The applications of computer vision range from submarine robotics, drone robotics, and even military applications, and experience with AI is also become more coveted in today's tech industry.
  - title: Minecraft Server Minigame Creation
    level: Intermediate
    description: Have you ever wondered how Minecraft servers, such as Hypixel, create minigames? In this workshop students will learn how the popular minigame, Skywars, is created and made functional. Throughout the workshop, students will learn the advanced uses of command blocks and redstone, as well as learn the function and syntax of advanced commands. Students will also be introduced to online resources that allow them to create their own minigames. This workshop will show how command blocks and redstone directly mimic the function of while loops, if-else statements, and even the creation and calling of methods!
  - title: Discord Chat Bot Programming
    level: Intermediate
    description: You've most likely heard of Discord, the popular social media messaging platform. If you've used it before, you have probably also used a Discord bot like Rythm Bot or MEE6. Ever wondered what goes into the development of these bots? In this workshop, you will learn all about what goes into programming a bot using discord.py and create various bots that enhance the way you can interact with others through discord.
  - title: Web Development with React and Firebase
    level: Advanced
    description: Web Development is one of the most in-demand skills today, and in this workshop, you'll learn how to use the hottest web development framework, React.js. You'll also learn how to utilize Firebase, a Google-created service, to create a fully-functioning database and backend. Using both React.js and Firebase, you'll create an entire website from scratch and deploy it! By the end of the workshop, you'll have all the skills necessary to create beatiful, robust, and functional websites! Previous experience with HTML, CSS, and JavaScript is recommended.
  - title: Intro to Cloud Computing and Architecture
    level: Advanced
    description: Cloud Computing is one of the hottest technologies today. Cloud is also one of the fastest growing technologies with enterprise adoption doubling every few years. With AWS being the current front runner in cloud technologies, This course will take you through the basics of cloud computing to set you apart from others in the today and future market.
  - title: Competitive Programming w/ USACO
    level: Advanced
    description: Want to apply your coding knowledge into a competitive, fun, and challenging platform? Consider trying Competitive Programming! Competitive programming involves using problem solving skills such as algorithms, math, and logic to solve a structured programming problem using programming. In this short workshop, I will introduce how Competitive Programming works and start by walking students through a simple problem on the USACO website. The language used will be Java. Prior coding experience required.
  - title: Machine Learning in Data Analysis
    level: Advanced
    description: Machine Learning is the next big thing in the tech industry. We see it all the time in from market predictions to finding patterns in unsolved mysteries. This course will teach you more than machine learning - it will teach you to think like an analyst. Coding experience recommended.
---

# **Welcome.**

We're ACE Coding — a dedicated group of high school students from Amador Valley High School dedicated to spreading the knowledge of programming to students of all ages. Every year, we host ACE Code Day, a free and open event where our teachers lead workshops on a variety of programming topics to enable students to further their knowledge in specialized areas of coding. Although ACE Code Day 2020 was canceled by COVID-19, we've worked hard to ensure this year's ACE Code Day is more engaging than ever. With nearly 20 individual volunteer-led workshops on topics ranging from computer hardware to computational fluid dynamics to machine learning, this year's ACE Code Day is also one of the largest in our history.

ACE Code Day is completely free and open to everyone, regardless of skill level. We have a workshop for everyone!

We would love for you to attend! Sign up below!
